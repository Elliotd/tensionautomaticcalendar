import requests
import json
from datetime import datetime
from util import month_filter_function
from util import deleted_cancelled_filter_function
from calendar_view import calendar_view

start_year = 2024
end_year = 2034

for i in range(start_year, end_year):
    for j in range(12):

        # Settings
        calendar_month = j+1
        calendar_year = i

        # Load token file
        f = open('token.json')
        token_data = json.load(f)
        f.close()

        url = 'https://api.momence.com/api/v1/Events'

        resp = requests.get(url=url, params=token_data)
        data = resp.json() # Check the JSON Response Content documentation below

        # filter for cancelled and deleted events
        filtered_data = list(filter(deleted_cancelled_filter_function, data))

        # Filter for the month
        filtered_data = list(filter(lambda x:month_filter_function(x,year=calendar_year, month=calendar_month), filtered_data))

        calendar = calendar_view("Calendars/" + str(calendar_year) + "-" + str(calendar_month) + ".svg", calendar_month, calendar_year)

        calendar = calendar_view("Calendar_out.svg", calendar_month, calendar_year)
        calendar.display_events(filtered_data)