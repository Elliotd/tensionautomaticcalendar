#!/usr/bin/env python

import math
import cairo
from cairo import SVGSurface
import calendar
from util import month_filter_function


# Creating function for make roundrect shape
def roundrect(context, x, y, width, height, r):
    context.arc(x+r, y+r, r,
                math.pi, 3*math.pi/2)
 
    context.arc(x+width-r, y+r, r,
                3*math.pi/2, 0)
 
    context.arc(x+width-r, y+height-r,
                r, 0, math.pi/2)
 
    context.arc(x+r, y+height-r, r,
                math.pi/2, math.pi)
 
    context.close_path()

# Variables for the apearence
BACKGROUND_COLOR = (0.16, 0.33, 0.46)
BACKGROUND_TRANSPARENCY = 0.5
LIGHT_COLOR = (0.96, 0.93, 0.86)
WIDTH = 1070 # Measured from Canva
HEIGHT = 1300 # Measured form Canva
SQUARE_INTER_SPACE = 10
SQUARE_RADIUS = 10
FONT = "Meta Serif Pro"
TITLE_FONT_SIZE = 40
TITLE_POSITION = (SQUARE_INTER_SPACE,SQUARE_INTER_SPACE)
TITLE_BOX_EXTENT = 20
NUMBER_FONT_SIZE = 30
EVENT_FONT_SIZE = 6
EVENT_LINE_SPACE = 40
DAY_NUMBER_DISTANCE_FROM_CORNER = (12,17)

# Actual constants
NCOLUMN = 7
NROW = 5
RECT_WIDTH = ((WIDTH - SQUARE_INTER_SPACE)/NCOLUMN)-SQUARE_INTER_SPACE
RECT_HEIGHT = ((HEIGHT -  SQUARE_INTER_SPACE)/NROW)-SQUARE_INTER_SPACE

class calendar_view:

    surface:SVGSurface
    month:int
    year:int


    def __init__(self, outfile:str, month:int, year:int):
        self.surface = cairo.SVGSurface(outfile, WIDTH, HEIGHT)
        self.month = month
        self.year = year
        self._create_background()
        self._create_labels()

    def _create_background(self):
        """This creates the background, margin and the squares of the calendar
        """
        # Create the colored background
        ctx = cairo.Context(self.surface)
        
        ctx.set_source_rgba(*BACKGROUND_COLOR, BACKGROUND_TRANSPARENCY)
        ctx.rectangle(0, 0, WIDTH, HEIGHT) 
        ctx.fill()

        # Add the white rounded squares$
        ctx.set_source_rgba(*LIGHT_COLOR)
        # Calculate size of rectangle
        for columns in range(NCOLUMN):
            for row in range(NROW):
                rectangle_x = columns*(RECT_WIDTH+SQUARE_INTER_SPACE) + SQUARE_INTER_SPACE
                rectangle_y = row*(RECT_HEIGHT+SQUARE_INTER_SPACE) + SQUARE_INTER_SPACE 
                roundrect(ctx, rectangle_x, rectangle_y, RECT_WIDTH, RECT_HEIGHT, 10)
                ctx.fill()

    def _create_labels(self):
        """This creates all the text of the background of the calendar
        """
        ctx = cairo.Context(self.surface)

        # Generate the numbers on the top left
        # find the day of the that month
        (first_day_weekday, nb_days) = calendar.monthrange(self.year, self.month)

        for day in range(nb_days):
            # Find row and column for day
            current_column = (day+first_day_weekday)%7
            current_row = math.floor((day+first_day_weekday)/7)

            # Find label position
            posx = (RECT_WIDTH + SQUARE_INTER_SPACE) * current_column + SQUARE_INTER_SPACE + DAY_NUMBER_DISTANCE_FROM_CORNER[0]
            posy = (RECT_HEIGHT + SQUARE_INTER_SPACE) * current_row + SQUARE_INTER_SPACE  + DAY_NUMBER_DISTANCE_FROM_CORNER[1]


            ctx.set_source_rgb(*BACKGROUND_COLOR)
            ctx.set_font_size(NUMBER_FONT_SIZE)
            ctx.select_font_face(FONT,
                                cairo.FONT_SLANT_NORMAL,
                                cairo.FONT_WEIGHT_NORMAL)
            (_, _, width, height, _, _) = ctx.text_extents(str(day+1))
            ctx.move_to(posx, posy + height/2)
            ctx.show_text(str(day+1))

    def display_events(self, events:list):
        """This function displays all the events on the right square in the calendar SVG file

        Args:
            events (dict): Dictionary containing all the events of this month only
        """
        ctx = cairo.Context(self.surface)

        (first_day_weekday, nb_days) = calendar.monthrange(self.year, self.month)
        for day in range(nb_days):
            # Get the events for that day
            filtered_event = list(filter(lambda x:month_filter_function(x,self.year, self.month, day+1), events))

            if len(filtered_event) == 0:
                continue

            # Find row and column for day
            current_column = (day+first_day_weekday)%7
            current_row = math.floor((day+first_day_weekday)/7)

            # find rectangle center
            center_x = current_column*(RECT_WIDTH+SQUARE_INTER_SPACE) + SQUARE_INTER_SPACE + RECT_WIDTH/2
            center_y = current_row*(RECT_HEIGHT+SQUARE_INTER_SPACE) + SQUARE_INTER_SPACE + RECT_HEIGHT/2

            # Get the total height and size of the text all
            total_height = EVENT_LINE_SPACE*(len(filtered_event)-1)
            
            # Add a bit of margin

            # Now place the text
            for i,event in enumerate(filtered_event):
                (_, _, width, height, _, _) = ctx.text_extents(event['title'])

                cursor_x = center_x - width/2
                cursor_y = center_y - total_height/2 + i * (EVENT_LINE_SPACE)
                
                ctx.move_to(cursor_x,cursor_y)
                ctx.show_text(event['title'])
                








      