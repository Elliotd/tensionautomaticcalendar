from datetime import datetime

def month_filter_function(event, year, month, day = None):
    date = datetime.strptime(event['dateTime'],"%Y-%m-%dT%H:%M:%S.000Z")
    if day is None:
        return date.month == month and date.year == year
    else :
        return date.day == day and date.month == month and date.year == year
    
def deleted_cancelled_filter_function(event):
    return not (event['isCancelled'] or event['isDeleted'])