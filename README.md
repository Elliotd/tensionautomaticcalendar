# Python calendar generator for tension calendar

## How to install

- Install VSCode
- Install Python 3.X
- Install the python extension in VSCode
- Install the [Meta Serif Pro Bold](https://www.azfonts.net/fonts/meta/extra-bold-2-303960) font. For some reason the tool cannot differentiate between the different style of the same font. Make sure only that one is installed.
- Clone the repository `git clone url`
- Make sure to ignore the token.json file modification as we don't want the token publically uploaded `git update-index --skip-worktree token.json`
- Add the correct token and host id to the token.json file
- Create python virtual environment (ctrl+shift+P / command+shift+P -> Python: Create Environment)
  - venv
  - choose desired python version
  - install requirements.txt `pip install -r requirements.txt`

## How to run

- In the main file, change the month and year accordingly.
- Run the main python script by pressing the green arrow icon on the top right
